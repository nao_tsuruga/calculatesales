package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品売上集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String SALES_AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_CODE_ERROR = "の支店コードが不正です";
	private static final String COMMODITY_CODE_ERROR = "の商品コードが不正です";
	private static final String SALE_FILE_INVALID_FORMAT = "売上ファイルのフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//コマンドライン引数が1つ設定されていなければエラー
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "商品", "^[0-9]{3}$")) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "支店", "^[a-zA-Z0-9]{8}$")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		//先にファイルの情報を格納する List(ArrayList) を宣言します。
		List<File> rcdFiles = new ArrayList<>();
		//フォルダ内のすべてのファイルをチェック
		for(int i = 0; i < files.length ; i++) {
			//売上ファイルの条件に当てはまったものだけ、List(ArrayList) に追加します。
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//売上ファイルを昇順に並び替え
		Collections.sort(rcdFiles);
		//売上ファイルが連番か比較
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			//ファイル名の頭8文字をintに変換
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			//売上ファイルが連番でなければエラー
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				List<String> lines = new ArrayList<>();

				while((line = br.readLine()) != null){
					lines.add(line);
				}
				//売上ファイルが3行か確認
				if(lines.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALE_FILE_INVALID_FORMAT);
					return;
				}
				//支店コードが一致しない場合はエラー
				if (!branchNames.containsKey(lines.get(0))) {
					System.out.println(lines.get(0) + BRANCH_CODE_ERROR);
					return;
				}
				//商品コードが一致しない場合はエラー
				if (!commodityNames.containsKey(lines.get(1))) {
					System.out.println(lines.get(0) + COMMODITY_CODE_ERROR);
					return;
				}
				//売上金額が数字ではなかった場合はエラー
				if(!lines.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//売上金額をStringからLongへ変換
				long fileSale = Long.parseLong(lines.get(2));
				//既にMapに入っている金額に新しい金額を足す
				Long branchSaleAmount = branchSales.get(lines.get(0)) + fileSale;
				//売上金額が11桁以上の場合、エラー
				if(branchSaleAmount >= 10000000000L){
					System.out.println(SALES_AMOUNT_OVER);
					return;
				}
				//支店別合計金額を上書きする
				branchSales.replace(lines.get(0), branchSaleAmount);

				//既にMapに入っている金額に新しい金額を足す
				Long commoditySaleAmount = commoditySales.get(lines.get(1)) + fileSale;
				//売上金額が11桁以上の場合、エラー
				if(commoditySaleAmount >= 10000000000L){
					System.out.println(SALES_AMOUNT_OVER);
					return;
				}
				//商品別合計金額を上書きする
				commoditySales.replace(lines.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap, String type,  String pattern) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(type + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				//1行ずつ","で区切って入力
				String[] items = line.split(",");
				//要素が2つではない、正規表現に当てはまらないものはフォーマットエラー
				if((items.length != 2) || (!items[0].matches(pattern))){
					System.out.println(type + FILE_INVALID_FORMAT);
					return false;
				}
				//Map に追加する2つの情報を put の引数として指定します。
				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], (long) 0);

			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//namesMapのKeyを順番にすべて取得
			for (String key : namesMap.keySet()) {
				//コード、名前、売上合計金額を入力
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				//改行
				bw.newLine();
			}
			bw.close();

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}